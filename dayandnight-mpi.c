#include <stdio.h>
#include <stdlib.h>
#include<mpi.h> 
#include <math.h>
#include <time.h>

int factor(int n){
    // return the largest factor that is not larger than sqrt(n)
    double sqr_root = sqrt(n);
    for(int i = sqr_root; i >= 1; i--){
        if(n % i == 0) return i;
    } 
}


void mpi_kernal(int High,int Width,int *tmp,int *tp)
{
	 
	
for(int t=0;t<10;t++)
	{	
	for(int i=0;i<High;i++)
	{
		for(int j=0;j<Width;j++)
		{
			int temp=tmp[(i-1)*Width+j-1]+tmp[(i-1)*Width+j]+tmp[(i-1)*Width+j+1]
					+tmp[i*Width+j-1] +tmp[i*Width+j+1]
					+tmp[(i+1)*Width+j-1]+tmp[(i+1)*Width+j]+tmp[(i+1)*Width+j+1];
			if(temp==3 || temp==4 || temp==6 || temp==7 || temp==8)
					tp[i*Width+j]=1;
			else 
				tp[i*Width+j]=0;	
		}
	}
	}
	
}	

int main(int argc, char** argv)
{
	int High =20;
	int Width =50;
	
    Width=High=atoi(argv[1]);
	 int my_rank = 0, comm_sz = 0;
   

	
	 MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Status status;
	if(comm_sz==1){
		
		int *Canvas1=(int*)malloc(sizeof(int)*(High*Width+2));   
		int *Canvas=(int*)malloc(sizeof(int)*(High*Width+2));
		 // Prepare data
 srand(time(NULL));
	 
	for(int i=0;i<High;i++)
	{
		for(int j=0;j<Width;j++)
		{
			Canvas1[i*Width+j]=rand()%2;  
		}
	}
		mpi_kernal(High,Width,Canvas1,Canvas);
		 // Output
        for(int i=0;i<High;i++)
		{
			for(int j=0;j<Width;j++)
			{
				printf("%d ",Canvas[i*Width+j]);
			}
		printf("\n");
	
		}
		  free(Canvas1);
		 
		  free(Canvas);
	}
	else{
	if(Width% (comm_sz - 1) != 0){
            Width-= Width% (comm_sz - 1);
            Width+= (comm_sz - 1);
    }

	
	int a = (comm_sz - 1) / factor(comm_sz - 1);
        int b = factor(comm_sz - 1);
        int each_row = Width/ a;
        int each_column = Width/ b;	 
	 if (my_rank == 0){
			int *Canvas1=(int*)malloc(sizeof(int)*(High*Width+2));   
			int *Canvas=(int*)malloc(sizeof(int)*(High*Width+2));
            // Prepare data
 srand(time(NULL));
	 
	for(int i=0;i<High;i++)
	{
		for(int j=0;j<Width;j++)
		{
			Canvas1[i*Width+j]=rand()%2;  
		}
	}
           
        for(int i = 1; i < comm_sz; i++){
                int beginRow = ((i - 1) / b) * each_row;
                int beginColumn = ((i - 1) % b) * each_column;  
            for(int j = 0; j < High; j++){
                MPI_Send(&Canvas1[j*High+beginColumn], each_column, MPI_INT, i, i * High + j + comm_sz + 2, MPI_COMM_WORLD);
			}
		} 

            // Recv: [beginRow:endRow, beginColumn:endColumn]
            for (int i = 1; i < comm_sz; i++){
                int beginRow = ((i - 1) / b) * each_row;
                int endRow = beginRow + each_row;
                int beginColumn = ((i - 1) % b) * each_column;
                for(int j = beginRow; j < endRow; j++){
                    MPI_Recv(&Canvas[j*Width+beginColumn], each_column, MPI_INT, i, each_row * i + (j - beginRow), MPI_COMM_WORLD, &status);
                }
            }

           

            // Output
        for(int i=0;i<High;i++)
		{
			for(int j=0;j<Width;j++)
			{
				printf("%d ",Canvas[i*Width+j]);
			}
		printf("\n");
	
		}
        
		  free(Canvas1);
		 
		  free(Canvas);
        }
        else{

           int *Canvas2=(int*)malloc(sizeof(int)*(High* each_column + 2));
         
		  int *partC=(int*)malloc(sizeof(int)*(each_row * each_column + 2));
          
            // Recv: 
            for(int j = 0; j < High; j++){
                MPI_Recv(&Canvas2[j * each_column + 0], each_column, MPI_INT, 0, my_rank * Width + j + comm_sz + 2, MPI_COMM_WORLD, &status);
            }
                
			mpi_kernal(each_row,each_column,Canvas2,partC);
			
            // Send: 
            for(int j = 0; j < each_row; j++){
                MPI_Send(&partC[j * each_column + 0], each_column, MPI_INT, 0, each_row * my_rank + j, MPI_COMM_WORLD);
            } 
 
		  free(Canvas2);
		  free(partC);
        }
	}
    

    MPI_Finalize();

	return 0;
}
